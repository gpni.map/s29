
db.inventory.insertMany([
		{
			"name": "Javascript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishers"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100,
			"publisher": "JS Publishing House"
		}
	]);



//Comparison Query Operators
//$gt operator 	- greater than operator
//				- Matches the values that are greater than a specified value
//$gte operator - matches tha values that are greater than or equal to a specified value.

/*
	Syntax:
		for $gt
		- db.collectionName.find({"field": {$gt: value}})
		for $gte
		- db.collectionName.find({"field": {$gte: value}})

*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
});

db.inventory.find({
	"stocks": {
		$gte: 50
	}
});


// $lt operator - matches the values that are less than a specified value
// $lte operator - matches the values that are less than or equal to a specified value

/*
	Syntax:
		for $lt
		- db.collectionName.find({"field": {$lt: value}})
		for $lte
		- db.collectionName.find({"field": {$lte: value}})
*/


db.inventory.find({
	"stocks": {
		$lt: 50
	}
});


db.inventory.find({
	"stocks": {
		$lte: 50
	}
});


// $ne operator - Not equal operator which matches all values that are not equal to a specified value

/*
	Syntax:
		- db.collectionName.find({"field": {$ne: value}})
*/

db.inventory.find({
	"stocks": {
		$ne: 50
	}
});


//$eq operator - Equal operator matches values that are equal to a specified value

/*
	Syntax:
		- db.collectionName.find({"field": {$eq: value}})
*/

db.inventory.find({
	"stocks": {
		$eq: 50
	}
});



// $in operator - Matches any of the values in an array

/*
	Syntax:
		- db.collectionName.find({"field": {$in: [value1, value2]}})
*/


db.inventory.find({
	"price": {
		$in: [5000, 10000]
	}
});



// $nin operator - Matches none of the values in an array

/*
	Syntax:
		- db.collectionName.find({"field": {$nin: [value1, value2]}})
*/


db.inventory.find({
	"price": {
		$nin: [5000, 10000]
	}
});


// to creat a range condition
db.inventory.find({
	"price": {$gt: 2000} && {$lt: 4000}
});


/*
	Mini-Activity:
		1. In the inventory collection, return all the documents that have the price equal or less than 4000.
		2. In the inventory collection, return all the documents that have stocks of 50 and 100

*/

db.inventory.find({
	"price": {$lte: 4000}
});


db.inventory.find({
	"stocks": {
		$in: [50, 100]
	}
});




//Logical Query Operators
/*
	$or operator - Joins query clauses with a logical OR operator and returns all documents that matches the conditions of their clauses

	Syntax:

		db.collectionName.find({
			$or: [
				{"fieldA": "valueA"},
				{"fieldB": "valueB"}
			]
		});
*/


db.inventory.find({
	$or: [
		{"name": "HTML and CSS"},
		{"publisher": "JS Publishing House"},
	]
});


db.inventory.find({
	$or: [
		{"author": "James Doe"},
		{"price": {
			$lte: 5000
		}}
	]
});


/*
	$and operator - Joins query clauses with a logical AND operator and returns all documents that matches the conditions of both clauses

	Syntax:

		db.collectionName.find({
			$and: [
				{"fieldA": "valueA"},
				{"fieldB": "valueB"}
			]
		});
*/


db.inventory.find({
	$and: [
		{"stocks": {
			$ne: 50
		}},
		{"price": {
			$ne: 5000
		}}
	]
});

//Without the $and operator
db.inventory.find({
		"stocks": {
			$ne: 50
		},
		"price": {
			$ne: 5000
		}
});



// Field projection
// - INCLUSION - it matches the document according tho the given criteria and returns included field/s
/* 
	Syntax:
		db.collectionName:find({criteria}, {"field": 1})	
*/

db.inventory.find(
	{
		"publisher": "JS Publishing House"
	},
	{
		"name": 1,
		"author": 1,
		"price": 1
	}
);


// - EXCLUSION - it matches the document/s with the given criteria and returns the fields that were not excluded
/* 
	Syntax:
		db.collectionName:find({criteria}, {"field": 1})	

		Syntax:
		db.collectionName:find({criteria}, {"field": 0})
*/

db.inventory.find(
		{
			"author": "Noah Jimenez"
		},
		{
			"price": 0,
			"stocks": 0
		}
	);



//REMINDER - we cant combine inclusion and exclusion except when hiding the _id.

db.inventory.find(
		{
			"price": {
				"$lte": 5000
			}
		},
		{
			"_id": 0,
			"name": 1,
			"author": 1
		},
	);



// Evaluation Query Operator
/*
	$regex operator (regular expression) - Selects/Matches documents where values match a specified regular expression

	Syntax:

		db.collectionName.find({
			"field": {
				$regex: 'pattern',
				$options: 'optionValue'
			}
		});

*/

//REMEMBER: Case sensitive if no options
db.inventory.find({
	"author": {
		$regex: 'M'
	}
});


// case insensitive
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i' //case insensitive code
	}
});



// Multiple letters in the pattern
db.inventory.find({
	"name": {
		$regex: 'dev',
		$options: '$i' //case insensitive code
	}
});


/*
	Mini Activity:
		- in the inventory collections, find a "name" of a book/s with letters "java" in it and has a "price" of greater than or equal to 5000.
			-- use $and, $regex and $gte operators

*/


db.inventory.find({
	$and:[
		{"name": {
			$regex: 'java',
			$options: '$i'
		}},
		{"price":  {
			$gte: 5000
		}
		}
	]
});


// Can we use query operators in other CRUD operations like updateOne, updateMany, deleteOne, deleteMany?
//ANS: Yes


/*
	updateOne({criteria/query}, {$set})
	updateMany({criteria/query}, {$set})
	deleteOne({criteria/query})
	deleteMany({criteria/query})
*/

db.inventory.updateOne(
		{"price": {
			$gte: 300
		}},
		{
			$set: {
				"stocks": 100
			}
		}
	);